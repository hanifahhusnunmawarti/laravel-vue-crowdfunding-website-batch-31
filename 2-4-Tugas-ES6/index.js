// SOAL 1

const luas = (panjang, lebar) => {
    return panjang * lebar;
}

let keliling = (panjang, lebar) => {
    return 2 * (panjang + lebar);
}

console.log(luas(8, 4));
console.log(keliling(8, 4));

//SOAL 2

const newFunction = (firstName, lastName) => ({
    firstName: firstName,
    lastName: lastName,
    fullName: () => console.log(firstName + " " + lastName)
})

//Driver Code 
newFunction("William", "Imoh").fullName()

//SOAL 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const { firstName, lastName, address, hobby } = newObject;
// Driver code
console.log(firstName, lastName, address, hobby);

//SOAL 4

const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

//Driver Code
console.log(combined)

//SOAL 5

const planet = "earth" 
const view = "glass" 
let before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`

console.log(before)


