<?php
abstract class Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    public function atraksi(){
        return $this->nama . 'sedang' . $this->keahlian;
    }
}

trait fight{
    public $attackPower;
    public $defencePower;

    public function serang($hewan){
        echo $this->nama . ' sedang menyerang ' . $hewan->nama;
        echo '<br>';
        $this->diserang($hewan);
    }

    public function diserang($hewan){
        echo $this->nama . ' sedang diserang ' . $hewan->nama;
        echo '<br>';
        echo 'Sisa Darah = ' . $this->darah = $this->darah - ($hewan->attackPower/$this->defencePower);
    }
}

class Elang extends Hewan{
    use fight;

    function __construct()
    {
        $this->nama = "Elang";
        $this->darah;
        $this->jumlahKaki = 2;
        $this->keahlian = "terbang tinggi";
        $this->attackPower = 10;
        $this->defencePower = 5;
    }

    public function getInfoHewan(){
        echo 'Jenis Hewan = ' . $this->nama;
        echo '<br>';
        echo 'Darah = '. $this->darah;
        echo '<br>';
        echo 'Jumlah Kaki = '. $this->jumlahKaki;
        echo '<br>';
        echo 'Keahlian = '. $this->keahlian;
        echo '<br>';
        echo 'AttackPower = '. $this->attackPower;
        echo '<br>';
        echo 'DefensePower = '. $this->defencePower;
    }
}

class Harimau extends Hewan{
    use fight;

    function __construct(){
        $this->nama = "Harimau";
        $this->darah;
        $this->jumlahKaki = 4;
        $this->keahlian = "lari cepat";
        $this->attackPower = 7;
        $this->defencePower = 8;
    }

    public function getInfoHewan(){
        echo '<br>';
        echo 'Jenis Hewan = ' . $this->nama;
        echo '<br>';
        echo 'Darah = '. $this->darah;
        echo '<br>';
        echo 'Jumlah Kaki = '. $this->jumlahKaki;
        echo '<br>';
        echo 'Keahlian = '. $this->keahlian;
        echo '<br>';
        echo 'AttackPower = '. $this->attackPower;
        echo '<br>';
        echo 'DefensePower = '. $this->defencePower;
        echo '<br>';
    }
}

$elang = new Elang();
$harimau = new Harimau();

echo "<br>";
$elang->getInfoHewan();
echo "<br>";
$elang->atraksi();
echo "<br>";
$elang->serang($harimau);

echo "<br>";
$harimau->getInfoHewan();
echo "<br>";
$harimau->atraksi();
echo "<br>";
$harimau->serang($elang);
?>